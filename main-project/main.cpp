#include <iostream>
#include <iomanip>

using namespace std;
#include "conference-program.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
//#include "processing.h"
void output(conference_program* subscriptions)
{
    /********** ����� ������� **********/
    cout << "������........: ";
    // ����� �������
    cout << subscriptions->reader.last_name << ' ';
    // ����� ������ ����� �����
    cout << subscriptions->reader.first_name[0] << '.';
    // ����� ������ ����� ��������
    cout << subscriptions->reader.middle_name[0] << '\n';
    cout << '\n';
    // ����� ��������
    cout << subscriptions->title << '\n';
    cout << '\n';
    /******** ����� ������ ������� ********/
    // ����� �����
    cout << setw(2) << setfill('0') << subscriptions->start.hour << ':';
    // ����� �����
    cout << setw(2) << setfill('0') << subscriptions->start.minute << '\n';
    cout << '\n';
    /********** ����� ����� ������� **********/
    // ����� �����
    cout << setw(2) << setfill('0') << subscriptions->finish.hour << ':';
    // ����� �����
    cout << setw(2) << setfill('0') << subscriptions->finish.minute << '\n';
    cout << '\n';
    cout << '\n';
cout << "Laboratory work #8. GIT\n";
cout << "Variant #8.Conference_program\n";
cout << "Author: Darya Checushco\n";
cout << "Group: 12\n";
return 0;
}
int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �8. ��������� �����������\n";
    cout << "�����: ������� �����\n";

    conference_program* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data1.txt", subscriptions, size);
        cout << "***** ��������� ����������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(conference_program*) = NULL;
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_by_author;
            cout << "*****������� �� ���*****\n\n";
            break;
        case 2:
            check_function = check_by_time;
            cout << "*****������� ������ 15 �����*****\n\n";
            break;
        default:
            throw "  ";
        }
        if (check_function)
        {
            int new_size;
            conference_program** filtered = filter(subscriptions, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}